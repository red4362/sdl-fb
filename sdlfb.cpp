#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>
#include <string>
#include <iostream>
#include <vector>
#include <filesystem>
#include <list>

bool pdfdump = false;
using std::filesystem::directory_iterator;
std::list<SDL_Surface*> images;
std::list<SDL_Surface*>::iterator imageIt;
std::list<SDL_Rect> rects;
std::list<SDL_Rect>::iterator rectIt;
using namespace std;
string pngStr = ".png";
string jpgStr = ".jpg";
string dump = "pdfdump";
SDL_Renderer *sdlRenderer;
SDL_Surface *gScreen;

SDL_Texture *loadTexture(string filename, SDL_Renderer *rendertex)
{
   SDL_Surface *loaded = IMG_Load(filename.c_str());
   SDL_Texture *tex;
   tex = SDL_CreateTextureFromSurface(rendertex, loaded);
   SDL_FreeSurface(loaded);
   return tex;
}

SDL_Surface *loadSurface(string filename, SDL_Surface *parScreen)
{
    SDL_Surface *loaded = IMG_Load(filename.c_str());
    if(loaded == NULL)
    {
        cout << "Could not load: " << filename << endl;
    }
    SDL_Surface *optimized = SDL_ConvertSurface(loaded, parScreen->format, 0);
    SDL_FreeSurface(loaded);
    return optimized;
}

SDL_Rect makeRect(int w, int h)
{
    SDL_Rect ret;
    ret.x = 0;
    ret.y = 0;
    ret.w = w;
    ret.h = h;
    return ret;
}

SDL_Point getTexSize(SDL_Texture *tex)
{
    SDL_Point size;
    SDL_QueryTexture(tex, NULL, NULL, &size.x, &size.y);
    return size;
}

int main(int argc, char *argv[])
{
   if(argc == 2)
   {
      if(argv[1] == dump)
      {
         pdfdump = true;
         cout << "PDF arg recieved" << endl;
      }
   }
    
   if(SDL_Init(SDL_INIT_VIDEO) != 0)
   {
      cout << "Was unable to init SDL" << endl;
      return 0;
   }

   SDL_Window *screen = SDL_CreateWindow("Test", 0,  0, 1024, 768, 0);
   gScreen = SDL_GetWindowSurface( screen );

   int fileCount = 0; 

   for(const auto & file : directory_iterator("."))
   {
      string fileName = file.path();
      size_t found = fileName.find(pngStr);
      if(pdfdump == true)
      {
           fileName = "page-" + std::to_string(fileCount) + ".png";
           SDL_Surface *image = loadSurface(fileName.c_str(), gScreen);
           images.push_back(image);
           cout << fileName << endl;
           fileCount++;
      }
      if(found != string::npos && pdfdump == false)
      {
         fileName = fileName.substr(2,string::npos);
         SDL_Surface *file = loadSurface(fileName.c_str(), gScreen);
         images.push_back(file);
         cout << fileName << endl;
      }
      found = fileName.find(jpgStr);
      if( found != string::npos && pdfdump == false)
      {
         fileName = fileName.substr(2,string::npos);
         SDL_Surface *file = loadSurface(fileName.c_str(), gScreen);
         images.push_back(file);
        cout << fileName << endl;
     }
   }
   //sdlRenderer = SDL_CreateRenderer(screen, -1 ,0);
   if(!screen)
   {
      cout << "Could not open window" << endl;
      return 0;
   }
   imageIt = images.begin();
   int imgflags = IMG_INIT_JPG | IMG_INIT_PNG;
   IMG_Init(imgflags);
   //SDL_Texture *myImage = loadtexture(argv[1], sdlRenderer);
   //SDL_Surface *mySurface = loadSurface(argv[1], gScreen);
   for(list<SDL_Surface*>::iterator imageIt = images.begin(); imageIt != images.end(); ++imageIt)
   {
      SDL_Rect stretch = makeRect((*imageIt)->w, (*imageIt)->h);
      stretch.x = 512 - (*imageIt)->w / 2;
      rects.push_back(stretch);
   }
   imageIt = images.begin();
   //SDL_Point sz = getTexSize(myImage);
   //SDL_Rect myRect;
   //myRect.x = 0; //Left corner
   //myRect.y = 480; //Bottom
   //myRect.w = sz.x; //Width of texture
   //myRect.w = 20;
   //myRect.h = sz.y; //Height of texture
   //myRect.h = 10;
   //SDL_SetRenderDrawColor(sdlRenderer, 0, 0, 0, 255);
   //SDL_RenderClear(sdlRenderer);
   //SDL_RenderCopy(sdlRenderer, myImage, NULL, NULL);
   //SDL_RenderPresent(sdlRenderer);
   bool quit = false;
   SDL_Event ev;
   rectIt = rects.begin();
   list<SDL_Surface*>::iterator imageListEnd = --images.end();
   while( !quit )
   {
      while( SDL_PollEvent( &ev ) )
      {
         switch( ev.type )
         {
            case SDL_KEYDOWN:
              if( ev.key.keysym.sym == SDLK_q)
              {
                quit = true;
              }
              if( ev.key.keysym.sym == SDLK_LEFT)
              {
                  if(imageIt != images.begin())
                  {
                     imageIt--;
                     rectIt--;
                  }
              }
              if( ev.key.keysym.sym == SDLK_RIGHT)
              {
                 if(imageIt != imageListEnd)
                 {
                    imageIt++;
                    rectIt++;
                 }
                 else if(imageIt == imageListEnd)
                 {
                    imageIt = images.begin();
                    rectIt = rects.begin();
                 }
              }
              break;
         } 
      }
      SDL_BlitScaled((*imageIt), NULL, gScreen, &(*rectIt));
      SDL_UpdateWindowSurface( screen );
   }
   //SDL_DestroyRenderer(sdlRenderer);
   for(imageIt = images.begin(); imageIt != images.end(); imageIt++)
   {
      SDL_FreeSurface((*imageIt));
   }
   SDL_FreeSurface(gScreen);
   SDL_DestroyWindow(screen);
   IMG_Quit();
   SDL_Quit();
   return 0;
}
